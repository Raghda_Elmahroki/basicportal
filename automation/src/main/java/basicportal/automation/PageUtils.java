package basicportal.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import io.appium.java_client.android.AndroidDriver;

public class PageUtils {
	
	private static HomePage homePage=null;
	private static WebDriver driver=null;
	private static WebDriverWait wait=null;
	
	public static void initializeBrowser(String browser) throws MalformedURLException {
		
		if(browser.equalsIgnoreCase(Target.FIREFOX_BROWSER.toString())) {
			System.setProperty("webdriver.gecko.driver", "/Users/raghdaelmahroki/git/basicPortal/automation/geckodriver");			
			driver=new FirefoxDriver();
			
		}else if (browser.equalsIgnoreCase(Target.CHROME_BROWSER.toString())){
			driver=new ChromeDriver();
		} 
		else if (browser.equalsIgnoreCase(Target.ANDROID_CHROME.toString())){
			DesiredCapabilities caps=new DesiredCapabilities();
			caps.setCapability("deviceName", "small");
			caps.setCapability("platformVersion", "6.0");
			caps.setCapability("appiumVersion", "1.5.3");
			caps.setCapability("app", "browser");
			caps.setCapability("browserName", "Browser");
			caps.setCapability("fullReset", true);
			caps.setCapability("newCommandTimeout", "7200");
			caps.setCapability("automationName", "Appium");

			driver=new AndroidDriver<WebElement>(new URL("http://ecp.ae/Arabic/Pages/default.aspx"),caps);
		} 	
		
			driver.manage().window().maximize();
			
			driver.get("http://ecp.ae/Arabic/Pages/default.aspx");
			wait = new WebDriverWait(driver, 60); 
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='LoginMenu']")));
		    homePage= PageFactory.initElements(driver, HomePage.class);
		    
		    
		
	}
		
		public static HomePage getHomePage() {
			return homePage;
		}
		
		public static WebDriver getWebDriver() {
			return driver;
		}
		public static WebDriverWait getWebDriverWait() {
			return wait ;
		}

}
