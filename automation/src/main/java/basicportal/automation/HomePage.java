package basicportal.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	
	public LoginPage goToLoginPage(WebDriver driver) {
		
		//TODO later change this to click on the buttons to reach login page
		driver.get("http://ecp.ae/Arabic/_layouts/15/IACAD.GeneralControls.Sharepoint/Login.aspx?ReturnUrl=/Arabic/_layouts/15/Authenticate.aspx");	
		WebDriverWait wait = new WebDriverWait(driver, 60); 
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id,'UserName')]")));
	    return PageFactory.initElements(driver, LoginPage.class);
	}

	
}
