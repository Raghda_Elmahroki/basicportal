package basicportal.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoggedInPage {

	public String getDisplayedUserName() {
		
		PageUtils.getWebDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(@class,'UserNameMsg')]")));
		
		return PageUtils.getWebDriver().findElement(By.xpath("//span[contains(@class,'UserNameMsg')]")).getText();
	}

}
