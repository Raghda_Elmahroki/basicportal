package basicportal.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	public void fillUserName(String username) {
		WebElement userNameField=PageUtils.getWebDriver().findElement(By.xpath("//input[contains(@id,'UserName')]"));
		userNameField.clear();
		userNameField.sendKeys(username);
	}
	
	public void fillPassword(String password) {
		WebElement passwordField=PageUtils.getWebDriver().findElement(By.xpath("//input[contains(@id,'password')]"));
		passwordField.clear();
		System.out.print("The password "+password);
		passwordField.sendKeys(password);
	}
	
	public LoggedInPage clickSubmit() {
		PageUtils.getWebDriver().findElement(By.xpath("//input[contains(@class,'btnLogin')]")).click();
		   return PageFactory.initElements(PageUtils.getWebDriver(), LoggedInPage.class);
	}
}
