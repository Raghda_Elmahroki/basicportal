package basicportal.automation;

import java.net.MalformedURLException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class TestDriver {
   LoginPage loginPage=null;
   LoggedInPage loggedInPage=null;
	
	
@BeforeTest( groups = { "login-positive","login-negative"})
	public void initTest() throws MalformedURLException{
	    // Write code here that turns the phrase above into concrete actions
		PageUtils.initializeBrowser((Target.FIREFOX_BROWSER).toString());
	 loginPage= PageUtils.getHomePage().goToLoginPage(PageUtils.getWebDriver());
	}

@Test( groups = { "login-positive"})
	public void loginTest(){
	    // Write code here that turns the phrase above into concrete actions
		String username="raghda";
		String password="Iacad@123";
		loginPage.fillUserName(username);
		loginPage.fillPassword(password);
		loggedInPage=loginPage.clickSubmit();
		 Assert.assertEquals("raghda", loggedInPage.getDisplayedUserName());
	}

@Test( groups = { "login-negative"})
public void loginTestToFail(){
    // Write code here that turns the phrase above into concrete actions
	String username="raghda";
	String password="Iacad@12";
	loginPage.fillUserName(username);
	loginPage.fillPassword(password);
	loggedInPage=loginPage.clickSubmit();
	 Assert.assertEquals("raghda", loggedInPage.getDisplayedUserName());
}
@AfterTest( groups = { "login-positive","login-negative"})
	public void tearTest() {
		PageUtils.getWebDriver().quit();
	}

}
